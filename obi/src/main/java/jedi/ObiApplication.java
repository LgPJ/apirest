package jedi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ObiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ObiApplication.class, args);
	}

}
