package jedi.repository;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import jedi.models.UsuarioModel;

@Repository
/*El UsuarioModel es el tipo de dato
el Long es el tipo de identificador, el identificador es el ID en la clase
 */
public interface UsuarioRepository  extends CrudRepository<UsuarioModel, Long>{
    public abstract ArrayList<UsuarioModel> findByPropiedad(Integer propiedad);
}
