package jedi.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jedi.models.UsuarioModel;
import jedi.repository.UsuarioRepository;

@Service
public class UsuarioService {
    
    @Autowired
    UsuarioRepository usuarioRepository;

    public ArrayList<UsuarioModel> obtenerUsuario(){
        
        return (ArrayList<UsuarioModel>) usuarioRepository.findAll();
    }

    public UsuarioModel guardarUsuario(UsuarioModel usuario){
        return usuarioRepository.save(usuario);
    }

    /*Con el metodo optional, se usa para que en la busqueda
    si no encuentra lo buscado, no pasa nigun problema */
    public Optional<UsuarioModel> obtenerPorId(Long id){
        return usuarioRepository.findById(id);
    }

    /*Con el este metodo buscamos el dato por propiedad, usando el metodo abstracto que se definio
    en el repositorio*/
    public ArrayList<UsuarioModel> obtenerPorPropiedad(Integer propiedad){
        return usuarioRepository.findByPropiedad(propiedad);
    }

    /*Eliminamos por Id */
    public boolean eliminarUsuario(Long id){
        try {
            usuarioRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
