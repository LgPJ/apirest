package jedi.models;

import javax.persistence.*;


@Entity
@Table(name = "usuario")
public class UsuarioModel {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    
    private String nombre;
    private String email;
    private Integer propiedad;
    
    public Long getId() {
        return id;
    }
    public Integer getPropiedad() {
        return propiedad;
    }
    public void setPropiedad(Integer propiedad) {
        this.propiedad = propiedad;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setId(Long id) {
        this.id = id;
    }
}
